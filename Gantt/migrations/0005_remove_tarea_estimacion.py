# Generated by Django 4.0 on 2022-02-01 05:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Gantt', '0004_tarea_estimacion_alter_tarea_responsable'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tarea',
            name='estimacion',
        ),
    ]
