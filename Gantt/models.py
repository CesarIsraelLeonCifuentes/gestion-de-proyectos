import logging
from datetime import datetime

from django.db import models


# Create your models here.
from django.utils import timezone


# Create your models here.

class Miembro(models.Model):
    nombre = models.CharField(max_length=200)


class Proyecto(models.Model):
    nombre = models.CharField(max_length=200)

    @classmethod
    def buscarProyectoById(cls, id):
        respuesta = Proyecto.objects.get(id=id)
        return respuesta


class Tarea(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    responsable = models.ForeignKey(Miembro, on_delete=models.DO_NOTHING)
    nombre = models.CharField(max_length=200)
    fechaInicio = models.DateTimeField()
    fechaFin = models.DateTimeField()
    estado = models.CharField(max_length=200)
    prioridad = models.IntegerField()

    @classmethod
    def agregarTarea(cls, idP, idR, nombre, fechaInicio, fechaFin, prioridad):
        try:
            proyecto = Proyecto.objects.get(id=idP)
            responsable = Miembro.objects.get(id=idR)
            format_data = "%Y-%m-%d"
            date1 = datetime.strptime(fechaInicio, format_data)
            date2 = datetime.strptime(fechaFin, format_data)
            inicio = datetime(date1.year, date1.month, date1.day, date1.hour, date1.minute, date1.second,
                              tzinfo=timezone.utc)
            fin = datetime(date2.year, date2.month, date2.day, date2.hour, date2.minute, date2.second,
                           tzinfo=timezone.utc)
            respuesta = Tarea(proyecto=proyecto,
                              responsable=responsable,
                              nombre=nombre,
                              fechaInicio=inicio,
                              fechaFin=fin,
                              estado='Sin estado',
                              prioridad=prioridad)
            respuesta.save()
            return True
        except Exception as Argument:
            logging.exception("Error al registrar la Tarea")
            return False
