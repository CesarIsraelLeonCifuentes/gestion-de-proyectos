from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('ingresoTareas/',views.irIngresoTareas,name='irIngresoTareas'),
    path('ingresarTareas/crear/', views.ingresarTareas,name='ingresar')
]