from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from Gantt.models import Tarea


def index(request):
    return render(request, 'index.html')


def irIngresoTareas(request):
    return render(request, 'ingresoTarea.html')


def ingresarTareas(request):
    if request.method == 'POST':
        Tarea.agregarTarea(idP="1",
                           idR="1",
                           nombre=request.POST.get('nombre'),
                           fechaInicio=request.POST.get('fechaInicio'),
                           fechaFin=request.POST.get('fechaFin'),
                           prioridad=request.POST.get('prioridad')
                           )
        return HttpResponseRedirect('/ingresoTareas/')
