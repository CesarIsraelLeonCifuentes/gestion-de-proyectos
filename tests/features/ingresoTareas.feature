# Created by CESAR at 10/01/2022
# language: es
Característica: Ingreso de tareas
  Como gerente del proyecto deseo poder agregar tareas al proyecto para que
  sean asignadas posteriormente a los empleados de manera particular.

  Esquema del escenario: : Ingreso de tareas
    Dado que el proyecto se ha creado
    Cuando se ingrese a la opción de registrar tareas
    Entonces se podrá agregar las siguientes tareas al proyecto con los atributos <nombre>, <fechaInicio>, <fechaFin>, <Prioridad>
    Ejemplos:
      | nombre                 | fechaInicio | fechaFin   | Prioridad |
      | Plan del Proyecto      | 2022-01-18  | 2022-02-22 | 10        |
      | Ejecucion del Proyecto | 2022-02-23  | 2022-08-27 | 9         |
      | Ejecucion de Pruebas   | 2022-08-28  | 2022-12-01 | 5         |


