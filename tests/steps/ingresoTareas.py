
from behave import *
from django.http import HttpRequest

from Gantt import views
from Gantt.models import Proyecto, Tarea
use_step_matcher("re")
@step("que el proyecto se ha creado")
def step_impl(context):
    respuesta = Proyecto.buscarProyectoById('1')
    assert respuesta.nombre == 'Proyecto Pruebas'


@step("se ingrese a la opción de registrar tareas")
def step_impl(context):
    respuesta = views.irIngresoTareas(HttpRequest())
    assert respuesta


@step("se podrá agregar las siguientes tareas al proyecto con los atributos (?P<nombre>.+), (?P<fechaInicio>.+), (?P<fechaFin>.+), (?P<Prioridad>.+)")
def step_impl(context, nombre, fechaInicio, fechaFin, Prioridad):
    """
    :type context: behave.runner.Context
    :type nombre: str
    """
    assert Tarea.agregarTarea(idP="1",
                                      idR="1",
                                      nombre=nombre,
                                      fechaInicio=fechaInicio,
                                      fechaFin=fechaFin,
                                      prioridad=Prioridad
                                      ) == True

